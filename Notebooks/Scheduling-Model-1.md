---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  argv: [/home/user/.repositories/telescope_scheduling/envs/telescope-scheduling/bin/python3,
    -Xfrozen_modules=off, -m, ipykernel_launcher, -f, '{connection_file}']
  display_name: Python 3 (telescope-scheduling)
  env: {}
  language: python
  metadata:
    debugger: true
  name: telescope-scheduling
  resource_dir: /home/user/.local/share/jupyter/kernels/telescope-scheduling
---

```{code-cell} ipython3
import numpy as np
import networkx as nx
import random as random
import itertools as it

def schedule(tiles,windows,teltimes):
    '''This will find a telescope schedule using the min-cost flow formulation.
    Inputs: 
        tiles: An list containing the probabilities for each tile
        windows: A list of lists where windows[i] represents the tiles are visible during window i
        teltimes: A list containing the list of windows that telescope j can view during i.e. teltimes[j] = the list of windows where telecope j can view
    Outputs:
        order: A list of dictionaries containing the tiles each telescope views i.e. order[j] = the dictionary for telescope j. order[j][i] = the tile viewed by telescope j at window i.
        '''
    G = nx.DiGraph()
    num_tels = len(teltimes)
    num_windows = len(windows)
    num_tiles = len(tiles)
    
    
    #Source, target, and no observation nodes
    G.add_nodes_from(['s','t','no'])
    #Telescope nodes
    G.add_nodes_from(['T'+str(i) for i in range(num_tels)])
    #Window nodes
    G.add_nodes_from(['W'+str(i) for i in range(num_windows)])
    #Tile nodes
    G.add_nodes_from(['I'+str(i) for i in range(num_tiles)])
    #No schedule
    G.add_node('no')
    
    Ns = [len(tel) for tel in teltimes]
    N = sum(Ns)
    
    #Add the demands
    G.nodes['s']['demand'] = -N
    G.nodes['t']['demand'] = N
    
    #Add the arcs from s to Ti
    G.add_edges_from([('s',"T"+str(i),{"capacity":Ns[i]}) for i in range(num_tels)])
    
    #Add the arcs from Ti to Wj
    for i in range(num_tels):
        G.add_edges_from([('T'+str(i),"W"+str(j),{"capacity":1}) for j in range(num_windows) if j in teltimes[i]])
    
    #Add the arcs from Wj to Ik
    costs = [1-t for t in tiles]
    #Scale costs and get ints in order for networkx to compute mincostflow
    scale = 1/min(costs)
    scosts = [int(1000000*cost) for cost in costs ]
    for j in range(num_windows):
        G.add_edges_from([('W'+str(j),"I"+str(k),{"weight":scosts[k]}) for k in range(num_tiles) if k in windows[j]])
        
    #Add the arcs from Wj to no
    M = max(scosts) + 1
    for j in range(num_windows):
        G.add_edges_from([('W'+str(j),"no",{"weight":M})])
        
    #Add arks from Ik to t
    G.add_edges_from([('I'+str(k),'t',{"capacity":1}) for k in range(num_tiles)])
    
    #Add the arc from no to t
    G.add_edges_from([('no','t',{"weight":0})])
    
    #Calculate and output the answer in the right format
    c,flowdict = nx.network_simplex(G)
    
    
    #Convert the output to the appropriate form (Can this be done cleaner?)
    order = []
    for i in range(num_tels):
        telplan = {}
        for j in teltimes[i]:
            #This ensures we keep a telescope scheduled for no more than one tile in a window
            used = False
            for k in range(num_tiles):
                try:
                    if flowdict['W'+str(j)]['I'+str(k)] >= 1 and not used:
                        telplan['W'+str(j)] = 'I'+str(k)
                        flowdict['W'+str(j)]['I'+str(k)] -= 1
                        used = True
                except:
                    pass
                    
        order.append(telplan)
    
    return order

def probsum(sched, tiles):
    '''Given a telescope scheduling and a list of probabliities by tile, return the sum of probabilities of the scheduled telescope plan.
    
    Inputs:
        sched: A telescope schedule from the schedule function
        tiles: A list of probabilities corresponding to each tile
    
    Outputs:
        tot: The total probability viewed in the plan
        '''
    
    tot=0
    for tel in sched:
        for key in tel:
            index = int(tel[key][1:])
            tot += tiles[index]
    
    return tot
```

```{code-cell} ipython3
G = nx.DiGraph()
G.add_nodes_from(['t','s','w'])
N=2
G.nodes['s']['demand'] = -N
G.nodes['t']['demand'] = N
G.nodes['w']['demand'] = 0
G.add_edge('s','t',capacity=2)
G.add_edge('t','w',capacity=0)
print(nx.min_cost_flow(G))
```

# Toy Problems #

```{code-cell} ipython3
#Prob 1
tiles1 = [0.3,0.01,0.02,0.07,0.06,0.04,0.1,0.005,0.005,0.07,0.02,0.2,0.025,0.075]
windows1 = [[0,1,2,3,4,5,6,7,8],[0,1,2,3,4,5,6,7,8],[3,4,5,6,7,8,9,10,11],[3,4,5,6,7,8,9,10,11],[5,6,7,8,9,10,11,12,13],[5,6,7,8,9,10,11,12,13]]
teltimes1 = [[0,1,3,4],[0,1,2,5]]

sched1 = schedule(tiles1,windows1,teltimes1)
print(sched1)
print(probsum(sched1,tiles1))
```

## Toy Problem 2 ##

For this generator, we need to be careful with the sizes we pick for some of the constraints:

tsize must be less than or equal to numwindows

wsize must be less than or equal to ntiles

```{code-cell} ipython3
#Prob 2

def toy2(ntiles=500, wsize=100, nwindows=100, tsize=70, ntels=3):
    '''This will generate random probability data and compute the schedule and probability sum for the schedule
    
    We need a couple of assumptions for this to work:
    1. tsize <= numwindows
    2. wsize <= ntiles
    
    Inputs:
        ntiles: The number of tiles to generate
        wsize: The number of tiles in each window
        nwindows: The number of windows in a viewing period
        tsize: The number of windows each telescope can view
        ntels: The number of telescopes
    
    Outputs:
        sched: The schedule of the telescopes, using the output format of the schedule function
        prob: The probabliity sum for the schedule
        
        '''

    tiles = [random.random() for i in range(ntiles)]
    s = sum(tiles)
    tiles = [tile/s for tile in tiles]
    tileindices = [i for i in range(len(tiles))]
    windows = [random.sample(tileindices,wsize) for i in range(nwindows)]
    windowindices = [i for i in range(nwindows)]
    teltimes = [random.sample(windowindices,tsize) for i in range(ntels)]

    sched = schedule(tiles,windows,teltimes)
    
    return sched, probsum(sched,tiles)

toy2 = toy2(ntiles=1000, wsize=400, nwindows=100, ntels=4)
print(toy2[1])
```

```{code-cell} ipython3

```

```{code-cell} ipython3

```
